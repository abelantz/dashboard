# i2C

As part of the recruitment process we require the following task to be completed.

# Task
- Create a simple dashboard to present the data within a structure that will be legible and easily digested by end-users.
- This can include as many charts as you feel is required, plus consideration towards chart type.
- It may also include text describing the charts but please do not analyse the data, simply use Lorem Ipsum as representation.
- Style the dashboard as you see fit. However, please bear in mind that we utilise [MaterialUI] in one of our projects.
- We would like to see the front end fetching the data via an API.

### Tech
While we want you to implement your own ideas and creativity to this task we do have a few restrictions due to our current stack:

- [ReactJS]
- [node.js]
- [express]
- [Mariadb] - MySql is fine also

So, please no Angular, NoSQL, etc. And definitely no jQuery for the ui/ux

### Data
You will find the following files:
```
+-- data\
|   +-- weekly.csv - difference between exposed/control results per week per product.
|   +-- top.csv - top line values for each metric, value uplift and % uplift
```

Typically, weekly data is presented using a line chart with the top-line values being displayed in either a bar chart or table format.

### Questions
Should you have any questions, please feel free to reach out via email.

[node.js]: <http://nodejs.org>
[express]: <http://expressjs.com>
[ReactJS]: <https://reactjs.org/>
[Mariadb]: <https://mariadb.org/>
[MaterialUI]: <https://www.material-ui.com/#/>


### Task Completed

### Instructions

In the project directory you can run

`npm install`

for installing node_modules

For installing dependencies in project directory you need to run:

`npm install chart.js`
`npm install express`
`npm install mysql`
`npm install react-chartjs-2`

and for Dev dependencies;
`npm install concurrently`
`npm install nodemon`

For installing dependencies in client directory you can run:

`npm install`
`npm install @material-ui/core`
`npm install axios`

For running web application you can run multiple scripts:

`npm run server` for running just server

`npm run dev` for running server and client at the same time

### Brainstorming

The reason for not creating api structure folder and api middlewares it was because i thought that would be overkilled for getting data from just two get request endpoints. I have not had any authentication process for using json web token and making post request to database

### Additional

Having studied the person specifications and job description for this role, I feel I have the skills, qualities and attributes to carry out the job to a very high standard. I also feel the role will give me a new fresh challenge, something which I have been looking for.

Having also studied your company in detail, there seems to be a very positive approach to the work you carry out and I also very much like the way you strive to deliver a high standard of customer care. High levels of customer care are not common nowadays, and certainly want to work in a company that looks after its customers as I enjoy seeing positive customer interaction and feedback within my job.

