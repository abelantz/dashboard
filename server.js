const express = require('express');
const mysql  = require('mysql');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'rootuser',
    database: 'product_details',
    port: '3306'
});

db.connect((err) => {
    if(err){
        throw err;
    }
    console.log('MySQL Connected')
})
const app = express();

app.get('/', (req, res) => res.send('API running'));

app.get('/api/weekly', (req, res) => {
    let sql = 'SELECT * FROM product_details.weekly';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        res.send(results);
    })
});

app.get('/api/top', (req, res) => {
    let sql = 'SELECT * FROM product_details.top';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        res.send(results);
    })
});

const PORT = process.env.PORT || 5001;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));