import React from 'react'
import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Layout from './containers/Layout'
import theme from './theme'


const App = () => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <div>
      <Layout />
    </div>
  </ThemeProvider>
)
export default App;