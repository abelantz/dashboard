import React from 'react';
import Grid from '@material-ui/core/Grid'
import Units from '../components/Top/Units'
import Spend from '../components/Top/Spend'
import Visits from '../components/Top/Visits'
import TotalCusts from '../components/Top/TotalCusts';
import Divider from '@material-ui/core/Divider';

const Top = () => {
    return(
        <div>
        <h1>Top values for each metric  </h1>
        <Divider />
        <Grid container spacing={3}>
          <Grid item xs={6}>
           <Units />
          </Grid>
          <Grid item xs={6}>
           <Spend />
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={6}>
           <Visits />
          </Grid>
          <Grid item xs={6}>
            <TotalCusts />
          </Grid>
        </Grid>
        <Divider />
        </div>
    );
}

export default Top;


