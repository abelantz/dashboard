import React from 'react'
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Aisle from './Weekly/Aisle'
import Offer from './Weekly/Offer'
import Brand from './Weekly/Brand'

const Weekly = () => {
    return(
        <div style={{paddingTop: '40px'}}>
        <h1>Difference between exposed/control results per week per product</h1>
        <Divider />
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Aisle />
          </Grid>
          <Grid item xs={12}>
            <Offer />
          </Grid>
          <Grid item xs={12}>
            <Brand />
          </Grid>
        </Grid>
        </div>
        
    );
}

export default Weekly