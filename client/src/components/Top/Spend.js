import React, {useState, useEffect} from 'react';
import {Bar} from 'react-chartjs-2';
import axios from 'axios';


const Spend = () => {
    const [chartData, setCharData] = useState({})

    const chart = () => {
        
        let expData = [];
        let ctrlData = [];
        let upData = [];
        let pctData = [];
        let mtData = [];
        let products = [];

         axios.get('/api/top')
            .then(response => {
                console.log(response.data);

                let d = response.data.filter(product => {
                    return product.METRIC === "Spend"
                });

                d.forEach(items => {

                    mtData.push(items.METRIC)
                    expData.push(items.EXPOSED)
                    ctrlData.push(items.CONTROL)
                    upData.push(parseFloat(items.UPLIFT))
                    pctData.push(parseFloat(items.PCT_UPLIFT))
                    products.push(items.PRODUCT)
                })
                console.log(d);
                setCharData({
                    labels: products,
                    datasets: [
                        {
                            label: 'EXPOSED',
                            data: expData,
                                backgroundColor: [
                                    'rgba(47, 69, 80, 0.6)',
                                    'rgba(47, 69, 80, 0.6)',
                                    'rgba(47, 69, 80, 0.6)'
                                ],

                            borderWidth: 2,
                        },
                        {
                            label: 'CONTROL',
                            data: ctrlData,
                            backgroundColor: [
                                'rgba(134, 187, 216, 0.6)',
                                'rgba(134, 187, 216, 0.6)',
                                'rgba(134, 187, 216, 0.6)'
                            ],
                            borderWidth: 2,
                           
                        },
                        {
                            label: 'UPLIFT',
                            data: upData,
                                backgroundColor: [
                                    'transparent',
                                ],
                                borderColor: [
                                    'black',
                                ],
                                borderWidth: 2,
                                type: 'line'
                        },
                        {
                            label: 'PCT_UPLIFT',
                            data: pctData,
                                backgroundColor: [
                                    'transparent',
                                ],
                                borderColor: [
                                    'red',
                                ],
                                borderWidth: 2,
                                type: 'line'
                        },
                        
                    ]
                })
            })
            .catch(err => {
                console.log(err)
            });
    }
    useEffect(() => {
        chart()
    }, [])
    return(
        <div className="App">
            <h2 style={{textAlign: 'center'}}>Spend</h2>

            <div>
                <Bar data={chartData} options={{
                    responsive: true
                }}/>
            </div>
        </div>
    )
}

export default Spend;