import React, {useState, useEffect} from 'react';
import {Line} from 'react-chartjs-2';
import axios from 'axios';


const Brand = () => {
    const [chartData, setCharData] = useState({})

    const chart = () => {

        let products = [];
        let dates = [];
        let expData = [];
        let ctrlData = [];
        let allData = [];
        axios.get('/api/weekly')
            .then(response => {
                console.log(response.data)
                let d = response.data.filter(product => {
                    return product.PRODUCT === "Brand"
                });
                d.forEach(items => {
                    allData.push(items)
                    products.push(items.PRODUCT)
                    expData.push(items.EXPOSED)
                    ctrlData.push(items.CONTROL)
                    dates.push(items.WEEK_COMMENCING)
                })
                

                setCharData({
                    labels: dates,
                    datasets: [
                        {
                            label: 'EXPOSED',
                            data: expData,
                            borderColor: [
                                'rgba(3, 252, 186, 1)'
                                
                            ],
                            backgroundColor: [
                                'transparent'
                            ],
                            borderWidth: 2
                        },
                        {
                            label: 'CONTROL',
                            data: ctrlData,
                            borderColor: [
                                'rgba(91, 52, 190, 0.6)'
                                
                            ],
                            backgroundColor: [
                                'transparent'
                            ],
                            borderWidth: 2
                        },
                        
                    ]
                })
            })
        
        
    }

    useEffect(() => {
        chart()
    }, [])

    return(
        <div className="App">
            <h1 style={{textAlign: 'center'}}>Brand</h1>
            <div>
                <Line data={chartData} options={{
                    responsive: true
                }}/>
            </div>
            
        </div>
    )
}

export default Brand;